//Daniel Deng dd66107

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class currencyPricingTest {

	//check if it gives the right rate
	@Test
	public void testCorrect() throws Exception {
		currencyPricing test = new currencyPricing();
		test.readData("rates.csv");
		
		double check = test.getFXQuote("USD", "AUD");
		assertEquals(0.9838, check, 0.01);
	}
	
	//test minor currency conversions
	@Test
	public void testMinor() throws Exception {
		currencyPricing test = new currencyPricing();
		test.readData("rates.csv");
		
		double check = test.getFXQuote("ZAR", "TWD");
		assertEquals(4.2921, check, 0.01);
	}
	
	
	//test that currency pairs are the same whether its x->y or y->x
	@Test
	public void testSimilarity() throws Exception {
		currencyPricing test = new currencyPricing();
		test.readData("rates.csv");
		
		double check1 = test.getFXQuote("CAD", "GBP");
		
		//do 1/x to get the inverse and ensure the rates are the same
		double check2 = 1/(test.getFXQuote("GBP", "CAD"));
		assertEquals(check1, check2, 0.01);
	}
}
