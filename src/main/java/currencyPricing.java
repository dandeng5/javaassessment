//Daniel Deng dd66107

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;



public class currencyPricing {

    String csvFile = "../../../rates.csv";
    Map<String, Double> ratesData = new HashMap<String, Double>();
    
	public void readData(String filePath) {
		
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        
       
        try {

            br = new BufferedReader(new FileReader(filePath));
            br.readLine();
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);
                
                //obtain the two different rate possibilities between any two currencies
                String ratesTicker1 = data[0] + data[1];
                String ratesTicker2 = data[1] + data[0];
                
                //if the currency pair exists already
                if (ratesData.containsKey(ratesTicker1) || ratesData.containsKey(ratesTicker2)) {
                	//if the pair is stored as is
                	if (ratesData.containsKey(ratesTicker1)) {
                		ratesData.put(ratesTicker1, Double.parseDouble(data[2]));
                    	System.out.println("Normal");
                	}
                	//if the pair is inversed
                	else {
                		ratesData.put(ratesTicker1, 1/ (Double.parseDouble(data[2])));
                    	System.out.println("Inversed");
                	}
                	System.out.println("This exists already");
                }
                //add the rate if it doesn't exist
                else {
                	ratesData.put(ratesTicker1, Double.parseDouble(data[2]));
                }
                System.out.println("First Country: " + data[0] + " , Second Country: " + data[1] + " , Rate: " + data[2]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	
	public double getFXQuote(String currency1, String currency2) throws Exception {
		String ratesTicker = currency1 + currency2;
		double finalRate = 0;
		
		try {
			finalRate = ratesData.get(ratesTicker);
		}
		catch (Exception e) {
			System.out.println("This rate does not exist");
			
			double rate1 = 0.0;
			double rate2 = 0.0;
			boolean flag1 = false;
			boolean flag2 = false;
			boolean flag3 = false;
			boolean flag4 = false;
			
			//deal with the conversion of two minor currencies
			if ((ratesData.containsKey(currency1 + "USD") || ratesData.containsKey("USD" + currency1)) && (ratesData.containsKey(currency2 + "USD") || ratesData.containsKey("USD" + currency2))) {
				//find rate1
				if (ratesData.containsKey(currency1 + "USD")) {
					rate1 = ratesData.get(currency1);
					flag1 = true;
				}
				else if (ratesData.containsKey("USD" + currency1)) {
					rate1 = ratesData.get(currency1);
					flag2 = true;
				}
				
				//find rate2
				if (ratesData.containsKey(currency2 + "USD")) {
					rate2 = ratesData.get(currency2);
					flag3 = true;
				}
				else if (ratesData.containsKey("USD" + currency1)) {
					rate2 = ratesData.get(currency2);
					flag4 = true;
				}
				
				
				//return rates
				if (flag1 == true && flag3 == true) {
					finalRate = ratesData.get(rate1) / ratesData.get(rate2);
					return finalRate;
				}
				else if (flag2 == true && flag3 == true) {
					finalRate = 1/(ratesData.get(rate2) / (1/ratesData.get(rate1))); 
					return finalRate;
				}
				else if (flag1 == true && flag4 == true) {
					finalRate = ratesData.get(rate1) * ratesData.get(rate2);
					return finalRate;
				}
				else if (flag2 == true && flag4 == true) {
					finalRate = ratesData.get(rate2) / ratesData.get(rate1);
					return finalRate;
				}
				else {
					System.out.println("Rate is not available for this pair of currencies!!!!");
					throw e;
				}
			}
		}
		finally {
			return finalRate;
		}
	}
	
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		double finalRate;
		
		System.out.println("Enter the first currency: ");
		String currency1 = reader.next().toUpperCase(); 
		
		System.out.println("Enter the second currency: ");
		String currency2 = reader.next().toUpperCase(); 
		
		currencyPricing test = new currencyPricing();
		test.readData("rates.csv");
		
		try {
			finalRate = test.getFXQuote(currency1, currency2);
			System.out.println("The conversion rate for " + currency1 + "-" + currency2 + " is " + finalRate);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		finally {
			//once finished
			reader.close();
		}
	}
}
